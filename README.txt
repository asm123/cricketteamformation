1. Import "CricketTeamFormation.zip" in NetBeans IDE.

2. The project has two Java files: 
	(1) Main.java: Run this file to run the code. It has two methods:
		(i) testWithSampleInput(): Tests the code on a sample input (given in Problem.txt), 
		(ii) testWithInputReadFromConsole(): Reads input from console in the following order and code is run on this input: 
			number of test cases
			for each test case:
				number of players
				array of players
				number of batsmen
				array of batsmen
				number of bowlers
				array of bowlers
				number of wicket-keepers
				array of wicket-keepers
				team size
				max number of batsmen in a team
				max number of bowlers in a team
				max number of wicket-keepers in a team
	(2) TeamCreator.java - Contains logic to form teams as per the given constraints. Its method formTeams() is called from Main.java. Javadoc is provided for each method in the file.

3. Output is printed to console.