package dream11.aug16;

import java.util.Scanner;

/**
 * Main class to test the implementation {@link TeamComposer}.
 * 
 * @author asmita
 */
public class Main {
    
    public static void main(String[] args) {
        testWithSampleInput();
//        testWithInputReadFromConsole();
    }
    
    /**
     * Call this method to test the code on a sample input (single test case)
     */
    private static void testWithSampleInput() {
        TeamCreator teamCreator;

        int teamSize, maxBatsmen, maxBowlers, maxWicketKeepers;

        int[] players = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] strength = {1, 1, 3, 1, 1, 3, 2, 2, 3, 4};
        int[] batsmen = {1, 2, 3};
        int[] bowlers = {4, 5, 6};
        int[] wicketKeepers = {7, 8, 9, 10};
        
        teamSize = 3;
        maxBatsmen = 1;
        maxBowlers = 1;
        maxWicketKeepers = 1;
        
        
        teamCreator = new TeamCreator(players, strength, batsmen, bowlers, wicketKeepers);
        teamCreator.setConstraints(teamSize, maxBatsmen, maxBowlers, maxWicketKeepers);
        teamCreator.formTeams();
    }
    
    /**
     *  Call this method read input from console and test the code on multiple test cases 
     */
    private static void testWithInputReadFromConsole() {
        TeamCreator teamCreator;
        
        Scanner scanner;
        int T; 
        int numberOfPlayers, numberOfBatsmen, numberOfBowlers, numberOfWicketKeepers;
        int[] players, strength, batsmen, bowlers, wicketKeepers;

        int teamSize, maxBatsmen, maxBowlers, maxWicketKeepers;
        
        scanner = new Scanner(System.in);
        T = scanner.nextInt();
        
        for (int t = 0; t < T; t++) {
            // array of players
            numberOfPlayers = scanner.nextInt();
            players = new int[numberOfPlayers];
            for (int i = 0; i < numberOfPlayers; i++) {
                players[i] = scanner.nextInt();
            }
            
            // array of strengths
            strength = new int[numberOfPlayers];
            for (int i = 0; i < numberOfPlayers; i++) {
                strength[i] = scanner.nextInt();
            }
            
            // array of batsmen
            numberOfBatsmen = scanner.nextInt();
            batsmen = new int[numberOfBatsmen];
            for (int i = 0; i < numberOfBatsmen; i++) {
                batsmen[i] = scanner.nextInt();
            }
            
            // array of bowlers
            numberOfBowlers = scanner.nextInt();
            bowlers = new int[numberOfBowlers];
            for (int i = 0; i < numberOfBowlers; i++) {
                bowlers[i] = scanner.nextInt();
            }
            
            // array of wicket keepers
            numberOfWicketKeepers = scanner.nextInt();
            wicketKeepers = new int[numberOfWicketKeepers];
            for (int i = 0; i < numberOfWicketKeepers; i++) {
                wicketKeepers[i] = scanner.nextInt();
            }
            
            // constraints
            teamSize = scanner.nextInt();
            maxBatsmen = scanner.nextInt();
            maxBowlers = scanner.nextInt();
            maxWicketKeepers = scanner.nextInt();
            
            
            teamCreator = new TeamCreator(players, strength, batsmen, bowlers, wicketKeepers);
            teamCreator.setConstraints(teamSize, maxBatsmen, maxBowlers, maxWicketKeepers);
            teamCreator.formTeams();
        }
        
    }
}
